require 'httparty'
require 'json'

module Owghat
    class DailyPrayers 
        def initialize(city="Tehran", country="Iran", method="0")
            @city = city 
            @country = country 
            @method = method 
        end 

        def timing_by_city
            request = HTTParty.get("http://api.aladhan.com/v1/timingsByCity?city=#{@city}&country=#{@country}&method=#{@method}")

            return JSON.parse(request.body)
        end
    end 
end